JustnifferLogParser
=============

[Andrey Zvorygin](http://vk.com/id828789), 2012

email: umount.dev.brain@ya.ru

___JustnifferLogParser___ - скрипт, который парсит логи Justniffer и записывает их в базу данных, благодаря чему в дальнейшем становится возможна удобная обработка данных об использовании интернет-канала на предприятии.

##Подготовка базы данных
Изначально скрипт рассчитан на работу с Postgresql. С помощью файла ***create_schema.sql*** можно создать таблицу в БД.

##Использование:

```shell
ruby init.rb [OPTIONS]
```

##Список параметров:

```-i, --file-to-load FILE```          Файл с логом Justniffer. По умолчанию используется файл justniffer.log в директории, в которой исполняется данный скрипт

```-d, --logfile-divider DIVIDER```    Разделитель параметров лог файла. По умолчанию /\t/ (символ табуляции)

```-t, --logfile-timestamp-format  FORMAT``` Формат, в котором записаны дата и время в логе. По умолчанию "%m/%d/%y %H:%M:%S"

```-f, --logfile_format ARRAY```       Массив с перечислением формата строки лог-файла. Перечисляются в том же порядке, как они идут в логе. По умолчанию ["request.timestamp", "source.ip", "source.port", "dest.ip", "dest.port", "request.size", "response.size", "request.header.host", "request.url"]

```-a, --db-adapter ADAPTER```         Адаптер Sequel для подключения к базе данных. По умолчанию postgres

```-s, --db-host HOST```               Хост сервера БД. По умолчанию localhost

```-n, --db-name NAME```               Имя базы данных. По умолчанию justniffer-log-parser

```-u, --db-user USER```               Имя пользователя БД. По умолчанию postgres

```-p, --db-password PASSWORD```       Пароль пользователя БД. По умолчанию не задан

```-h, --help```                       Показать список параметров

##Варианты значений, которые можно передать в параметре --logfile-format

**close_time** (real)

**close_timestamp** (timestamp)

**close_timestamp2** (reltime)

**connection_value** (character varying) *(в параметрах justniffer значится как **connection**)*

**connection_time** (real)

**connection_timestamp** (timestamp)

**connection_timestamp2** (reltime)

**idle_time_0** (real)

**idle_time_1** (real)

**response_time_begin** (real)

**response_time_end** (real)

**dest_ip** (character varying)

**dest_port** (integer)

**source_ip** (character varying)

**source_port** (integer)

**request** (text)

**request_timestamp** (timestamp)

**request_timestamp2** (reltime)

**request_size** (integer)

**request_line** (text)

**request_method** (character varying)

**request_url** (text)

**request_protocol** (character varying)

**request_header** (text)

**request_header_host** (character varying)

**request_header_user_agent** (text)

**request_header_accept** (text)

**request_header_authorization** (text)

**request_header_connection** (character varying)

**request_header_content_encoding** (character varying)

**request_header_content_length** (integer)

**request_header_content_md5** (character varying)

**request_header_cookie** (text)

**request_header_range** (text)

**request_header_referer** (text)

**request_header_keep_alive** (text)

**request_header_transfer_encoding** (text)

**request_header_via** (text)

**response** (text)

**response_timestamp** (timestamp)

**response_timestamp2** (reltime)

**response_size** (integer)

**response_time** (real)

**response_line** (text)

**response_protocol** (text)

**response_code** (integer)

**response_message** (text)

**response_header** (text)

**response_header_server** (character varying)

**response_header_date** (text)

**response_header_content_length** (integer)

**response_header_content_type** (character varying)

**response_header_content_md5** (character varying)

**response_header_content_encoding** (character varying)

**response_header_content_language** (character varying)

**response_header_transfer_encoding** (character varying)

**response_header_etag** (character varying)

**response_header_cache_control** (character varying)

**response_header_last_modified** (character varying)

**response_header_pragma** (character varying)

**response_header_age** (integer)

**response_header_connection** (character varying)

**response_header_keep_alive** (character varying)

**response_header_via** (character varying)

**response_header_vary** (character varying)

**response_header_accept_ranges** (character varying)

**response_header_set_cookie** (text)