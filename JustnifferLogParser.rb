# encoding: utf-8

require "date"
require "sequel"

Sequel.connect @options[:db]

class JustnifferLogRecord < Sequel::Model
end

class JustnifferLogParser
  attr_reader :logfile, :logfile_divider, :logfile_format, :script_dir

  def initialize(params = {})
    # Файл должен быть читаемым и не быть директорией
    raise "File corrupted: #{params[:file_to_load]}" unless File.file?(params[:file_to_load]) and File.readable?(params[:file_to_load])
    @logfile = File.new(params[:file_to_load])
    @logfile_format = params[:logfile_format]
    @divider = params[:logfile_divider]
  end

  def parse_log_file()
    @logfile.each do |line|
      line_splitted = line.strip!.split(@divider)
      # Количество записей в строке лога должно быть равно ожидаемому количеству аргументов
      raise "Logfile format corrupted" unless line_splitted.size == @logfile_format.size
      # Подготовим данные для добавления в БД
      line_to_db = prepare_db_data Hash[@logfile_format.zip line.split(@divider)]
      # Забросим данные в БД, если они там, конечно, еще не присутствуют
      JustnifferLogRecord.find_or_create line_to_db
    end
  end

  private
  def convert_to_date(params = {})
    params[:format] ||= "%m/%d/%y %H:%M:%S"
    DateTime.strptime(params[:value], params[:format]).strftime "%Y-%m-%d %H:%M:%S"
  end

  # Приходится числа опять конвертить в строки, потому что иначе Sequel не обрамит значение в кавычки,
  # а postgresql не разрешит искать данные, не обрамленнеы кавычками
  def convert_to_integer(params = {})
    params[:value]
  end

  def convert_to_real(params = {})
    params[:value]
  end

  def prepare_db_data(data_hash = {})
    # Правила обработки полей. Справа - атрибуты, слева - функции, которые будут вызываться, когда эти атрибуты попадутся
    columns_rules = {
      :convert_to_date => [:close_timestamp, :connection_timestamp, :request_timestamp, :response_timestamp],
      :convert_to_integer => [:dest_port, :source_port, :request_size, :request_header_content_length, :response_size, :response_code, :response_header_content_length, :response_header_age],
      :convert_to_real => [:close_time, :connection_time, :idle_time_0, :idle_time_1, :response_time_begin, :response_time_end, :response_time]
    }
    # Разберем хеш на части, будем проверять каждую
    data_hash.each do |key, value|
      matched_rules = columns_rules.select do |a, b|
        b.include? key
      end
      matched_rules.each do |key1, value1|
        # puts "#{key} --- #{value} --- #{key1}"
        # Вызовем функцию обработки значения
        data_hash[key] = send(key1, :value => value)
      end
    end
    # Готово. Вернем обработанный хеш
    data_hash
  end
end