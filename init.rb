# encoding: utf-8

require 'optparse'

# Хеш опций
@options = {}

optparse = OptionParser.new do|opts|
  opts.banner = "Использование: init.rb [OPTIONS]"

  @options[:file_to_load] = [Dir[File.expand_path(File.dirname(__FILE__))], "justniffer.log"].join("/")
  opts.on('-i', '--file-to-load FILE', 'Файл с логом Justniffer. По умолчанию используется файл justniffer.log в директории, в которой исполняется данный скрипт') do |file|
    @options[:file_to_load] = file
  end

  @options[:logfile_divider] = /\t/
  opts.on('-d', '--logfile-divider DIVIDER', 'Разделитель параметров лог файла. По умолчанию /\t/ (символ табуляции)') do |divider|
    @options[:logfile_divider] = divider
  end

  @options[:logfile_timestamp_format] = "%m/%d/%y %H:%M:%S"
  opts.on('-t', '--logfile-timestamp-format FORMAT', 'Формат, в котором записаны дата и время в логе. По умолчанию "%m/%d/%y %H:%M:%S"') do |format|
    @options[:logfile_timestamp_format] = format
  end

  @options[:logfile_format] = [:request_timestamp, :source_ip, :source_port, :dest_ip, :dest_port, :request_size, :response_size, :request_header_host, :request_url]
  opts.on('-f', '--logfile-format ARRAY', 'Массив с перечислением формата строки лог-файла. Перечисляются в том же порядке, как они идут в логе. По умолчанию "request_timestamp source_ip source_port dest_ip dest_port request_size response_size request_header_host request_url"') do |format|
    @options[:logfile_format] = (format.split " ").map { |key| key.to_sym }
  end

  @options[:db] = {}
  
  @options[:db][:adapter] = "postgres"
  opts.on('-a', '--db-adapter ADAPTER', 'Адаптер Sequel для подключения к базе данных. По умолчанию postgres') do |adapter|
    @options[:db][:adapter] = adapter
  end

  @options[:db][:host] = "localhost"
  opts.on('-s', '--db-host HOST', 'Хост сервера БД. По умолчанию localhost') do |host|
    @options[:db][:host] = host
  end

  @options[:db][:database] = "justniffer-log-parser"
  opts.on('-n', '--db-name NAME', 'Имя базы данных. По умолчанию justniffer-log-parser') do |name|
    @options[:db][:database] = name
  end

  @options[:db][:user] = "justniffer-log-parser"
  opts.on('-u', '--db-user USER', 'Имя пользователя БД. По умолчанию postgres') do |user|
    @options[:db][:user] = user
  end

  opts.on('-p', '--db-password PASSWORD', 'Пароль пользователя БД. По умолчанию не задан') do |password|
    @options[:db][:password] = password
  end

  opts.on('-h', '--help', 'Показать этот экран') do
    puts opts
    puts "\nВарианты значений, которые можно передать в параметре --logfile_format:

-----------------------------------------------------------------
Значение                                       Тип в БД          
-----------------------------------------------------------------
close_time                                     real              
close_timestamp                                timestamp         
close_timestamp2                               reltime           
connection_value                               character varying
connection_time                                real
connection_timestamp                           timestamp
connection_timestamp2                          reltime
idle_time_0                                    real
idle_time_1                                    real
response_time_begin                            real
response_time_end                              real
dest_ip                                        character varying
dest_port                                      integer
source_ip                                      character varying
source_port                                    integer
request                                        text
request_timestamp                              timestamp
request_timestamp2                             reltime
request_size                                   integer
request_line                                   text
request_method                                 character varying
request_url                                    text
request_protocol                               character varying
request_header                                 text
request_header_host                            character varying
request_header_user_agent                      text
request_header_accept                          text
request_header_authorization                   text
request_header_connection                      character varying
request_header_content_encoding                character varying
request_header_content_length                  integer
request_header_content_md5                     character varying
request_header_cookie                          text
request_header_range                           text
request_header_referer                         text
request_header_keep_alive                      text
request_header_transfer_encoding               text
request_header_via                             text
response                                       text
response_timestamp                             timestamp
response_timestamp2                            reltime
response_size                                  integer
response_time                                  real
response_line                                  text
response_protocol                              text
response_code                                  integer
response_message                               text
response_header                                text
response_header_server                         character varying
response_header_date                           text
response_header_content_length                 integer
response_header_content_type                   character varying
response_header_content_md5                    character varying
response_header_content_encoding               character varying
response_header_content_language               character varying
response_header_transfer_encoding              character varying
response_header_etag                           character varying
response_header_cache_control                  character varying
response_header_last_modified                  character varying
response_header_pragma                         character varying
response_header_age                            integer
response_header_connection                     character varying
response_header_keep_alive                     character varying
response_header_via                            character varying
response_header_vary                           character varying
response_header_accept_ranges                  character varying
response_header_set_cookie                     text"
    exit
  end
end
optparse.parse!

require_relative 'JustnifferLogParser'
p = JustnifferLogParser.new @options
p.parse_log_file